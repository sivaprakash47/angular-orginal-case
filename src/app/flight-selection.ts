import { Airport } from "./airport-search-box/airport";

export class FlightSelection{
    private readonly direction: FlightDirection;
    private airport: Airport = null;
    private airportChangeCallBack: () => void;

    constructor(direction: FlightDirection, airportChangeCallback: ()=>void ) {
        this.direction = direction;
        this.airportChangeCallBack = airportChangeCallback;
    }

    public setAirport(airport: Airport): void {
        this.airport = airport;
        this.airportChangeCallBack();
    }

    public getAirportCode(): string {
        return this.airport.code;
    }

    public getAirportDesc(): string {
        return this.airport.description;
    }

    public isOriginSelection(): boolean {
        return this.direction === FlightDirection.ORIGIN;
    }

    public isDestinationSelection(): boolean {
        return this.direction === FlightDirection.DESTINATION;
    }

    public hasSelection(): boolean {
        return this.airport !== null;
    }
}

export enum FlightDirection {
  ORIGIN = 'Origin...',
  DESTINATION = 'Destination...',
}