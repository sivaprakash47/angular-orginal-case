import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Fare } from './fare';

@Injectable({
  providedIn: 'root'
})
export class FareService {
  private faresUrl: string = 'http://localhost:9000/travel/book/fare';

  constructor( private http: HttpClient) { }

  public getFare(originCode: string, destinationCode: string): Observable<Fare> {
    debugger;
    const url = `${this.faresUrl}/${originCode}/${destinationCode}`;
    return this.http.get<Fare>(url);
  }

}
