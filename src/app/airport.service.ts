import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Airport } from './airport-search-box/airport'

import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AirportService {

  private airportsUrl: string = 'http://localhost:9000/travel/book';

  constructor( private http: HttpClient) { }

  getAirports(): Observable<Airport[]> {
    return this.http.get<Airport[]>(this.airportsUrl);
  }

  getAriportsByTerm(term: string): Observable<Airport[]>{
    const url = `${this.airportsUrl}/header/${term}`;
    return this.http.get<Airport[]>(url);
  }

  getAirportByKey(key: string): Observable<Airport> {
    const url = `${this.airportsUrl}/header/${key}`;
    return this.http.get<Airport>(url);
  }
}
