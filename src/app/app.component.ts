import { Component, Input } from '@angular/core';
import { FlightSelection, FlightDirection } from './flight-selection';
import { FareService } from './fare.service';
import { Fare } from './fare';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  private origin: FlightSelection = new FlightSelection(FlightDirection.ORIGIN, () => this.onFlightSelected());
  private destination: FlightSelection = new FlightSelection(FlightDirection.DESTINATION, () => this.onFlightSelected());

  private fare: Fare = null;
  
  constructor(private fareService: FareService) { }

  public onFlightSelected(){
    if(this.isBothSelected()) {
      this.fareService.getFare(this.originCode(), this.destinationCode())
      .subscribe(fare => this.fare = fare);
    }
      
  }

  private isBothSelected(): boolean {
    return this.origin.hasSelection() && this.destination.hasSelection();
  }

  private originCode(): string {
    return this.origin.getAirportCode();
  }

  private destinationCode(): string {
    return this.destination.getAirportCode();
  }

}